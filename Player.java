package oxgame;

public class Player {
    private char name;
    private int win;
    private int lose;
    private int draw;
    
    Player(char name){
        this.name = name;
        win = 0;
        lose = 0;
        draw = 0;
    }
    
    public int getName(){
        return name;
    }
    public int getWin(){
        return win;
    }
    public int getLose(){
        return lose;
    }
    public int getDraw(){
        return draw;
    }
    public void increaseWin(){
        win++;
    }
    public void increaseLose(){
        lose++;
    }
    public void increaseDraw(){
        draw++;
    }
}


